Belkin-Buster is (as of now) a PC-based GUI application aimed at exploiting
some algorithms known to crack wireless access points sold by a company known
as Belkin Wireless. For the most part, Belkin-Buster is a stand-alone app, and
will work on Windows, Linux, and will soon be executable on Macbooks.

Be sure to note the LICENSE, DISCLAIMER, & TERMS/CONDITIONS. This version of
Belkin-Buster will not allow you to use the application, unless you agree to
the TERMS/CONDITIONS/DISCLAIMER. Modifying my code does not void you of this
agreement. I will not be held responsible for your actions.

This is an ongoing project, and will continue to be updated by me throughout
the rest of this year, and perhaps furthermore if I see fit.

Contact me via ...
    
    EMAIL: Cipher.Foxx@gmail.com
    FACEB: CipherFoxx
    TWITR: (coming soon)

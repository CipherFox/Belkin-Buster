import os, sys, subprocess, re, string, platform, socket, tkinter as tk
from tkinter import ttk, messagebox
from datetime import datetime
from uuid import getnode
from tkinter import *
'''
    (1) Review each label's/text's font-style & size throughout the entire program.
    (2) During proximity-scans, write in code to also store an access-point's following attributes: signal, security(WEP/WPA(1/2)), encryption.
    (3) Rearrange all frames, labels, & widgets to where they're packed into the environment, rather than gridded/placed.
    (4) Implement a status-based 'scanning/loading' animation for the Scan-page, to execute (only) during execution of the scan method.
    (5) IMPLEMENT SOMETHING TO DEAL WITH GUEST (OPEN/NOT-OPEN) ACCESS-POINTS ...
    (6) Review the compatability/inter-operability of iconbitmap/iconphoto (for all platforms).
    (7) NEW ADDITION FOR SCAN PAGE (AFTER ALL FIXES = DONE):
            When collecting credentials (MAC, SSID, PASS, etc), also collect the max/min
		    SNR (signal). If a connection is below 70%, have an interactive exclamation-
		    point appear that, when clicked on or hovered over, will display a message
		    including ALL credentials - ALONG WITH A MESSAGE STATING THAT THE CONNECTION
		    IS < 70% AND MAY NOT PERMIT SYNC, AND THAT THE USER SHOULD MINIMIZE THE DISTANCE
		    BETWEEN HIM/HER AND THE PRIMARY ROUTER.
    (8) Every class begins with the exact same 4-6 lines of code. Implement a (global) function that allows simplification via minimizing lines used.
'''
class app(tk.Tk):

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)
        self.wm_title('Belkin-Buster') # is displayed on all pages?
        tk.Tk.info = [[],[],[],[],['024613578ACE9BDF', '944626378ace9bdf', '51274063', '0123456789abcdef', ''], str()] # 0:wmac, 1:ssid, 2:pass, 3:eventlog, 4:keys, 5:physAddr
        for i in range(0, 12, 2):
            tk.Tk.info[5] += hex(getnode())[2:][i : i + 2] + ':'
        tk.Tk.info[5] = tk.Tk.info[5][:-1]
        container = Frame(self)
        container.pack(expand = 1, fill = BOTH, side = TOP)
        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)
        self.frames = {}
        for page in (Agreement, Home, Scan, Input, Log):
            PAGE = page.__name__
            frame = page(parent = container, controller = self)
            self.frames[PAGE] = frame
            frame.grid(row = 0, column = 0, sticky = 'nsew')
        menus = []
        m_bar = tk.Menu(self)
        m_map = [['Open Containing Folder', 'Save/Export Log', 'Load/Import Log', '', 'Save Current Session', 'Load Recent Session', '', '', 'Exit'],['Copy to Clipboard ->', ''],['Color Styles', 'Label Styles', 'Create/Upload Styles ->', 'References ...', 'Loading Animation ...'], ['Preferences ...', 'Default-Directory ...', 'Save/Load Current Theme ...', 'Notifications ...', 'Discovery-Defaults ...', 'Developer Options ...'], ['About ->', 'Requirements', 'Compatibility', 'Troubleshoot ...', '', 'Contact Author', '', 'Donate ->'],['File', 'Edit', 'View', 'Settings', '?']]
        for i in range(5):
            menus.append(tk.Menu(self, m_bar, tearoff = 0))
            for x in range(len(m_map[i])):
                if m_map[i][x] == '':
                    menus[i].add_separator()
                else:
                    menus[i].add_command(label = m_map[i][x], command = quit) if i == 0 and x == 8 else menus[i].add_command(label = m_map[i][x])
            m_bar.add_cascade(label = m_map[5][i], menu = menus[i])
        tk.Tk.config(self, menu = m_bar)
        self.show_frame('Agreement')
    
    def show_frame(self, PAGE):

        for frame in self.frames.values():
            frame.grid_remove()
        frame = self.frames[PAGE]
        frame.winfo_toplevel().geometry('420x470') if str(frame) == '.!frame.!agreement' else frame.winfo_toplevel().geometry('330x325')
        frame.grid()
    
    def log(self, src, msg):

        return datetime.now().strftime('%m/%d/%Y %H:%M:%S') + ' [' + src + '] - ' + msg

    def gen_f(tget, bgclr, wdth, hght):

        return Frame(tget, background = bgclr, width = wdth, height = hght) # attempt, review/rename, etc.

    def gen_l(tget, txt, bgclr, fgclr, fnt, jstfy):

        return Label(tget, text = txt, background = bgclr, foreground = fgclr, font = fnt, justify = jstfy)
    
    def gen_b(tget, txt, cmd):
    
        return ttk.Button(tget, text = txt, command = cmd) # ttk.Button ?

class Agreement(tk.Frame):
    
    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.configure(background = 'black')
        self.winfo_toplevel().resizable(0, 0)
        app.info[3].append(app.log(app, 'BELKINBUSTER', 'STARTED USER={' + app.info[5] + '}'))
        app.gen_l(self, 'TERMS & CONDITIONS', 'black', 'blue', ('Arial bold', 14), 'center').pack(anchor = 'c', pady = 15, side = TOP)
        self.terms = str("Belkin International Inc. is an American manufacturer of consumer electronics\nthat specializes in connectivity devices. It was founded on April 18, 1983 in\nHawthorne, California by Chet Pipkin. Belkin International is the parent-company\nfor Belkin, Linksys, and Wemo-branded products and services.\n\nBy continuing, you agree and swear under penalty of perjury that the sole developer,\nAustin B Carter, is hereby and henceforth dismissed of ANY & EVERY consequence\nderived from your actions/intentions in using this application, its features, and the\nlogic within it/them. The software and implemented logic within this application\nqualifies as 'sensitive', and in some cases - possibly 'dangerous', information.\n\nBreaching any forms of personal, private, public, and/or enterprise networks,\nwhether protected or unprotected, without prior knowledge and permission\nof/from the network\'s owner(s), is a federal offense and punishable via\nup to but not limited to 10 years of federal imprisonment.\n\nBy continuing and accepting the declared terms of use, you will hereby & henceforth\nbe binded by these terms, and you also understand, agree, and swear under penalty\nof perjury that the developer of this application/program is permanently waived and\ndismissed of/from any/every instance of consequence derived from your intentions\nand/or actions in using this application.\n\nTerms of use & developer-disclaimers may be modified and/or deleted whenever and\nhowever the developer sees fit. Modifications & deletions of the terms/disclaimers\nDO NOT dismiss/waive ANY previous/current users from ANY prior/current terms.\n\nIf you DO NOT agree with (or lack ANY understanding of) any one of\nthe terms above, then YOU ARE NOT PERMITTED to use this application.\n\nIf you DO understand & agree to the terms, select \'I Agree\' to continue.\n")
        app.gen_l(self, self.terms, 'black', 'red', ('Arial bold', 7), 'center').pack(anchor = 'c', side = TOP)
        self.ans = app.gen_f(self, 'black', 200, 25)
        #self.ans.pack_propagate(0)
        self.buttons = []
        for i in range(2):
            self.buttons.append(app.gen_b(self.ans, 'I Disagree' if i else 'I Agree', self.deny if i else lambda:controller.show_frame('Home')))
            self.buttons[i].pack(padx = 10, side = RIGHT if i else LEFT)
        self.ans.pack(anchor = 'c', pady = 15, side = TOP)
    
    def deny(self):

        app.info[3].append(app.log(app, 'DISCLAIMER', 'EVENT (!) USER {' + app.info[5] + '} DENIED BINDING AGREEMENT !'))
        messagebox.showwarning('Terminating ...', 'You must accept the agreement to use Belkin-Buster.')
        app.quit()

class Home(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.configure(background = 'black')
        self.winfo_toplevel().resizable(0, 0)
        app.info[3].append(app.log(app, 'DISCLAIMER', 'EVENT (!) USER {' + app.info[5] + '} ACCEPTED BINDING AGREEMENT !'))
        self.h_title = app.gen_f(self, None, 330, 112); self.h_title.pack_propagate(0)
        self.H = [['grey','white','white','grey',165,165,159,158,115,115,55,55,0,165,8,165,0,0,20,20],['BELKINBUSTER','AUSTIN','CARTER\'s','Wireless Password-Cracker','black','grey','white','black','white','white','grey','red',('Arial',30),('Courier New bold',8),('Courier New bold',8),('Courier New bold',10),10,115,165,65,22,0,0,80]]
        
        # Find a better way to do the following ...
        for i in range(4):
            app.gen_f(self.h_title, self.H[0][i], self.H[0][i + 4], self.H[0][i + 8]).place(x = self.H[0][i + 12], y = self.H[0][i + 16])
        for i in range(4):
            app.gen_l(self.h_title, self.H[1][i], self.H[1][i + 4], self.H[1][i + 8], self.H[1][i + 12], 'center' if i == 3 else None).place(x = self.H[1][i + 16], y = self.H[1][i + 20])
        
        self.h_title.pack(side = TOP)
        app.gen_l(self, 'Choose an option below ...', 'black', 'blue', ('Arial bold', 8), 'center').pack(fill = X, pady = 20, side = TOP)
        self.body = app.gen_f(self, 'black', 265, 125); self.body.pack_propagate(0)
        self.rbp = [[],[],['Scan', 'Input', 'Log', 'Exit']]
        self.attr = [['Automatic', 'Manually', ('Arial bold', 8), ('Courier New bold', 8), 'green2', 'black'],['Proximity Scan', 'Input SSID/BSSID', 'Navigate to the Log', 'Exit the Application']]

        # Find a better way to do create/place buttons after doing so with frames (below) ...
        for i in range(4):
            self.rbp[0].append(app.gen_f(self.body, 'black', None, None))
            self.rbp[1].append(app.gen_b(self.rbp[0][i], self.rbp[2][i], quit if i == 3 else lambda i = i:controller.show_frame(self.rbp[2][i])))
            self.rbp[1][i].pack(padx = 10, side = LEFT)
            if i < 2:
                app.gen_l(self.rbp[0][i], self.attr[0][i], 'black', 'green2', self.attr[0][2], None).pack(side = LEFT)
            app.gen_l(self.rbp[0][i], self.attr[1][i], 'black', 'green2', self.attr[0][3], None).pack(side = LEFT)
            self.rbp[0][i].pack(anchor = 'w', pady = 3)
        self.body.pack(padx = 15)

class Scan(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.configure(background = 'black')
        self.winfo_toplevel().resizable(0, 0)
        self.s_title = app.gen_f(self, 'black', 330, 70); self.s_title.pack_propagate(0)
        self.tmp = []
        for i in range(2):
            self.tmp.append(app.gen_l(self.s_title, 'Belkin-Buster\'s' if i else 'Wifi Scanner', 'black', 'blue' if i else 'white', ('Arial bold', 8 if i else 30), None))
            self.tmp[i].place(x = 35, y = 0) if i else self.tmp[i].pack(fill = X, pady = 10)
        self.s_title.pack(anchor = 'c', pady = 3, side = TOP)
        self.catg = ['MAC', 'SSID', 'PASS']
        self.mains = app.gen_f(self, 'gray7', 240, 115); self.mains.pack_propagate(0)
        self.subs = [[],[]]
        for i in range(4):
            self.subs[0].append(app.gen_f(self.mains, 'black', 240 if i < 1 else 80, 25 if i < 1 else 115))
            self.subs[0][i].pack_propagate(0)
        for i in range(3):
            self.subs[1].append(app.gen_f(self.subs[0][0], 'black', 80, 25))
            self.subs[1][i].pack_propagate(0)
            app.gen_l(self.subs[1][i], self.catg[i], 'black', 'white', ('Arial bold', 9), None).pack()
            self.subs[1][i].pack(side = LEFT)
        self.subs[0][0].pack(anchor = 'c', fill = X, side = TOP)
        self.loot = [[],[],[]] # check back with ...
        for i in range(3):
            for x in range(5):
                self.loot[i].append(app.gen_l(self.subs[0][i + 1], 'EMPTY', 'black', 'red', ('Arial', 7), None))
                self.loot[i][x].pack(anchor = 'c', fill = X, side = TOP)
            self.subs[0][i + 1].pack(side = LEFT)
        self.mains.pack(anchor = 'c', side = TOP)
        self.counts = app.gen_f(self, 'black', 330, 22); self.counts.pack_propagate(0)
        self.count = []
        self.counted = []
        for i in range(2):
            self.count.append(app.gen_f(self.counts, 'gray7', 83 if i else 87, 22)) # review
            self.count[i].pack_propagate(0)
        for i in range(2):
            app.gen_l(self.count[i], ('Keys' if i else 'SSIDs') + 'Found:', 'black', 'white', ('Arial', 8), None).pack(padx = 1, pady = 1, side = LEFT)
            self.counted.append(app.gen_l(self.count[i], '0', 'black', 'red', ('Courier New bold', 8), None))
            self.counted[i].pack(pady = 1, side = LEFT)
            self.count[i].place(x = 166 if i else 77)
        self.counts.pack(fill = X, pady = 10, side = BOTTOM)
        '''
        self.actions = app.gen_f(self, 'black', 180, 80); self.actions.pack_propagate(0) # framesize: width = 180 height = 80 (parent)
        self.bttns = [] # container (parent)
        for i in range(3):
            self.bttns.append(app.gen_f(self.actions, 'black', 60, 80))
            self.bttns[i].pack_propagate(0)
        self.push = { 0 : self.start, 1 : self.decipher, 2 : self.stop }
        self.b_lbls = ['Start', 'Hack', 'Stop']
        app.gen_b(self.bttns[0], 'Back', lambda:controller.show_frame('Home')).pack(anchor = 'c', side = LEFT)
        app.gen_b(self.bttns[2], 'Reset', self.reset).pack(side = TOP)
        for i in range(3):
            app.gen_b(self.bttns[1], self.b_lbls[i], self.push[i]).pack(padx = 1, pady = 1, side = TOP)
        for i in range(3):
            self.bttns[i].pack(side = LEFT)
        self.actions.pack(side = BOTTOM)
        '''
        # Check accuracy on other OSs ...
        self.actions = app.gen_f(self, 'black', 180, 81)
        #self.actions.pack_propagate(0)
        self.bttns = []
        for i in range(3):
            self.bttns.append(app.gen_f(self.actions, 'black', 180, 27))
        self.push = { 0 : (lambda:controller.show_frame('Home')), 1 : self.decipher, 2 : self.reset }
        self.b_lbls = ['Back', 'Hack', 'Clear']
        app.gen_b(self.bttns[0], 'Start', self.start).pack(side = BOTTOM)
        app.gen_b(self.bttns[2], 'Stop', self.stop).pack(side = TOP)
        for i in range(3):
            app.gen_b(self.bttns[1], self.b_lbls[i], self.push[i]).pack(side = LEFT)
            self.bttns[i].pack(side = TOP)
        self.actions.pack(side = BOTTOM)
        
        # animation below - via threading (do) ...
        '''
        self.wdgt = Canvas(self, width = 30, height = 30, background = 'yellow')
        self.wdgt.place(x = 280, y = 30)
        self.blobs = [[],[]]
        self.spd = [[0,0],[0,0]]
        '''
    
    def update_UI(self):

        self.update()
        if len(app.info[0]) == 0:
            print(0)
            '''
            self.reset()
            messagebox.showwarning('No Belkin connections were detected.', 'Minimize your distance from the access-point and rescan.')
            self.stop()
            '''
        else:
            for i in range(2):
                self.counted[i].configure(text = len(app.info[i + 1]), foreground = 'green2' if i else 'dodger blue')
                self.count[i].configure(background = 'green2' if i else 'dodger blue')
            for i in range(5):
                if i < len(app.info[0]):
                    for x in range(3):
                        if x == 2 and len(app.info[2][i]) == 8:
                            self.loot[x][i].configure(text = app.info[x][i], foreground = 'green2', font = ('Arial bold', 7))
                        else:
                            self.loot[x][i].configure(text = app.info[x][i], foreground = 'dodger blue' if i else 'blue', font = ('Arial bold', 7))
                else:
                    for x in range(3):
                        self.loot[x][i].configure(text = 'EMPTY', foreground = 'red', font = ('Arial', 7))
        if self.scanning:
            self.loading()
            self.after(1000, self.update_UI)
        else:
            self.wdgt.place_forget()
    
    def update(self):

        self.info = [[],[],[],['netsh wlan show networks bssid', 'nmcli -f ssid,bssid -p dev wifi list', 'airport -s', (platform.system() != 'Windows') + (platform.system() == 'Darwin')]]
        self.cmd = subprocess.getoutput(self.info[3][self.info[3][3]])
        self.info[1] = re.findall(r'\welkin.+', self.cmd)
        for f in range(2):
            for o in range(len(self.info[1])):
                for x in range(len(self.cmd.split())):
                    if self.info[1][o] == self.cmd.split()[x]:
                        self.info[2 if f else 0].append(self.cmd.split()[x] + 1 if self.info[3][3] else 14)
        for i in range(3):
            for x in self.info[i]:
                if self.info[i] not in app.info:
                    app.info[i].append(x)
    
    def decipher(self):

        for i in range(len(app.info[2])):
            app.info[2][i] = re.sub(':', '', app.info[2][i][6:14] + str(hex((int(app.info[2][i][15]) * 16) + (self.info[1][0] == 'b') + int(app.info[2][i][16]))).lstrip('0x'))
            for o in range(8):
                app.info[4][4] += app.info[2][i][int(app.info[4][2][o])]
            app.info[2][i] = app.info[4][4]
            app.info[4][4] = str()
            for o in range(len(app.info[2][i])):
                for x in range(16):
                    if app.info[2][i][o] == app.info[4][3][x]:
                        app.info[4][4] += app.info[4][app.info[1][i][0] == 'b'][x]
                        break
            app.info[2][i] = app.info[4][4]
        #for i in range(len(app.info[2])):
            #self.loot[2][i].configure(text = app.info[2][i], foreground = 'green2', font = ('Arial bold', 7))
        app.info[4][4] = str()
    
    def reset(self):

        for i in range(3): # make sure this isn't a bad idea ...
            app.info[i].clear()
            for x in range(5):
                self.loot[i][x].configure(text = 'EMPTY', foreground = 'red', font = ('Arial', 7))
        for i in range(2):
            self.count[i].configure(background = 'gray7')
            self.counted[i].configure(text = '0', foreground = 'red')
    
    def loading(self):

        for i in range(0, 30, 15):
            self.blobs[int(i / 15)] = self.wdgt.create_oval(i, i, i + 15, i + 15, fill = 'blue' if i else 'red')
            self.loc = self.wdgt.coords(self.blobs[int(i / 15)])
            self.wdgt.move(self.blobs[int(i / 15)], self.spd[int(i / 15)][0], self.spd[int(i / 15)][1])
            if self.loc[0] == 0 and self.loc[1] == 0 and self.spd[int(i / 15)][0] == 0:
                self.spd[int(i / 15)] = [5, 0]
            elif self.loc[2] == 30 and self.loc[1] == 0 and self.spd[int(i / 15)][1] == 0:
                self.spd[int(i / 15)] = [0, 5]
            elif self.loc[3] == 30 and self.loc[2] == 30 and self.spd[int(i / 15)][0] == 0:
                self.spd[int(i / 15)] = [-5, 0]
            elif self.spd[int(i / 15)] == 30 and self.loc[0] == 0 and self.spd[int(i / 15)][1] == 0:
                self.spd[i] = [0, -5]

    def start(self):

        app.info[3].append(app.log(app, 'SCANNER', 'STARTED USER={' + app.info[5] + '}'))
        self.scanning = 1
        self.update_UI()
    
    def stop(self):

        app.info[3].append(app.log(app, 'SCANNER', 'STOPPED USER={' + app.info[5] + '}'))
        self.scanning = 0

class Input(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.configure(background = 'black')
        self.winfo_toplevel().resizable(0, 0)
        self.input = app.gen_f(self, 'black', 330, 260); self.input.pack_propagate(0)
        self.i_title = app.gen_f(self.input, 'black', 295, 74); self.i_title.pack_propagate(0)
        for i in range(2):
            app.gen_l(self.i_title, 'MAC-Decrytper' if i else 'Belkin-Buster\'s', 'black', 'white' if i else 'red', ('Arial bold', 30 if i else 12), None).place(x = i, y = 20 if i else i)
        self.i_title.place(x = 17, y = 20)
        self.body = app.gen_f(self.input, 'red', 289, 140); self.body.pack_propagate(0)
        self.dir = app.gen_f(self.body, 'gray5', 285, 22); self.dir.pack_propagate(0)
        self.dir.pack(pady = 2) # ???
        app.gen_l(self.dir, 'Enter the Required Information Below', 'gray5', 'orange red', ('Arial bold', 10), None).pack()
        self.main = app.gen_f(self.body, 'gray5', 285, 75); self.main.pack_propagate(0)
        self.vals = [[],[]]
        for i in range(2): # review this loop .....
            self.vals[0].append(app.gen_l(self.main, 'BSSID' if i else 'SSID', 'black', 'yellow', ('Arial bold', 11), None).place(x = 182 if i else 55, y = 10))
            self.vals[1].append(Entry(self.main, background = 'black', foreground = 'white', justify = 'center'))
            self.vals[1][i].place(x = 145 if i else 15, y = 40, width = 120) # what ... ??? ...
        self.main.pack()
        self.b_frame = app.gen_f(self.body, 'gray7', None, None)
        self.b_funcs = { 0 : (lambda:controller.show_frame('Home')), 1 : self.decrypt, 2 : self.reset } # check accuracy of execution ...
        self.b_names = ['Back', 'Get Password', 'Reset']
        for i in range(3):
            app.gen_b(self.b_frame, self.b_names[i], self.b_funcs[i]).pack(padx = 0 if i % 2 else 13, pady = 5, side = LEFT)
        self.b_frame.place(x = 2, y = 103)
        self.body.place(x = 20, y = 110)
        self.input.pack()
        self.output = [Label(self), Label(self)]
    
    def decrypt(self):

        app.info[3].append(app.log(app, 'INPUT', '*HACKING* DECRYPTION BEGINNING ...'))
        self.values = []
        for i in range(3):
            self.values.append(str(self.vals[1][0 if i == 0 else 1].get()))
        if len(self.values[0]) >= 10 and len(self.values[0] <= 13):
            if len(self.values[1]) == 17:
                self.values[1] = re.sub(':', '', self.values[1][6:14] + str(hex((int(self.values[1][15]) * 16) + (self.values[0][0] == 'b') + int(self.values[1][16]))).lstrip('0x'))
                for o in range(8):
                    app.info[4][4] += self.values[1][int(app.info[2][o])]
                self.values[1] = app.info[4][4]
                app.info[4][4] = str()
                for o in range(len(self.values[1])):
                    for x in range(16):
                        if self.values[1][o] == app.info[4][3][x]:
                            app.info[4][4] += app.info[4][self.values[0][0] == 'b'][x]
                            break
                self.values[1] = app.info[4][4]
                app.info[4][4] = str()
                for o in range(2):
                    output[o] = app.gen_l(self, self.values[1] if o else 'The default password is', 'black', 'green2' if o else 'cyan', ('Arial bold', 11) if o else ('Courier New', 10), None)
                if len(self.values[1]) == 8:
                    for o in range(2):
                        self.output[o].pack()
                    app.info[3].append(app.log(app, 'INPUT', 'SSID=(' + self.values[0] + ') BSSID=(' + self.values[2] + ') KEY=(' + self.values[1] + ')'))
            else:
                app.info[3].append(app.log(app, 'INPUT', 'ERROR (!) INVALID MAC-ADDRESS: ' + self.values[2]))
                for x in range(2):
                    self.output[x].configure(text = 'Please enter a valid BSSID!', foreground = 'red', font = ('Courier New bold', 10))
        else:
            app.info[3].append(app.log(app, 'INPUT', 'ERROR (!) INVALID SSID: ' + self.values[0]))
            for x in range(2):
                self.output[x].configure(text = 'Please enter a valid SSID!', foreground = 'red', font = ('Courier New bold', 10))
    
    def reset(self):

        app.info[4][4] = str()
        for i in range(2):
            self.vals[1][i].delete(0, END)
            self.output[i].configure(text = None, foreground = None)
        app.info[3].append(app.log(app, 'INPUT', 'EVENT (-) USER CLEARED ALL FIELDS'))

class Log(tk.Frame):

    def __init__(self, parent, controller):

        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.configure(background = 'black')
        self.winfo_toplevel().resizable(0, 0)
        app.gen_l(self, 'SESSION - LOG', 'black', 'dark orange', ('Times', 16, 'bold'), None).pack(anchor = 'c', pady = 15, side = TOP)
        self.b_frame = app.gen_f(self, 'black', 300, 40); self.b_frame.pack_propagate(0)
        self.tmp = [['Back', 'Export', 'Clear'],['n', 'n', 'c'],['left', 'right', 'top']]
        self.l_funcs = { 0 : self.goHome, 1 : self.exportlog, 2 : self.clear }
        for i in range(3):
            app.gen_b(self.b_frame, self.tmp[0][i], self.l_funcs[i]).pack(anchor = self.tmp[1][i], side = self.tmp[2][i])
        self.b_frame.pack(side = BOTTOM)
        self.l_frame = app.gen_f(self, None, 300, 200)
        self.l_frame.pack_propagate(0)
        self.scrs = [Scrollbar(self.l_frame, orient = HORIZONTAL), Scrollbar(self.l_frame)]
        self.logbx = Listbox(self.l_frame, width = 100, height = 100, xscrollcommand = self.scrs[0].set, yscrollcommand = self.scrs[1].set)
        for i in range(2):
            self.scrs[i].config(command = self.logbx.yview if i else self.logbx.xview)
            self.scrs[i].pack(fill = Y if i else X, side = RIGHT if i else BOTTOM)
        self.logbx.pack(fill = BOTH, expand = 1, side = LEFT)
        for i in range(len(app.info[3])):
            self.logbx.insert(END, " " + app.info[3][i] + " ")
        self.l_frame.pack()
        self.start()

    def clear(self):

        app.info[3].clear() # check on this ...
        self.logbx.delete(0, END)
        app.info[3].append(app.log(app, 'LOG', 'EVENT (-) USER={' + app.info[5] + '} CLEARED DATA-LOG'))
    
    def update(self):

        if int(getint(self.tk.call(self.logbx, 'size'))) != len(app.info[3]):
            for i in range(int(getint(self.tk.call(self.logbx, 'size'))), len(app.info[3])):
                self.logbx.insert(END, (' ' + app.info[3][i] + ' '))
        if self.logging:
            self.after(1000, self.update)
    
    def goHome(self):

        self.currPage = 0 # ???????
        self.controller.show_frame('Home')
    
    def start(self):
        
        self.logging = 1
        self.update()
    
    def exportlog(self):

        app.info[3].append(app.log(app, 'BELKINBUSTER', '* EXPORTING LOG TO PATH=(' + subprocess.getoutput('echo %cd%') + ') *'))
        self.build = '\n *** [BELKIN-BUSTER LOGDUMP DATE/TIME: ' + datetime.now().strftime('%m/%d/%Y %H:%M:%S') + '] *** [!]\n\n'
        for i in range(len(app.info[3])):
            self.build += (' ' + app.info[3] + '\n')
        self.dump = open('log_' + datetime.now().strftime('%m%d%Y') + '.txt', 'a')
        self.dump.write(self.build)
        self.dump.close()



BELKINBUSTER = app()
BELKINBUSTER.mainloop()
